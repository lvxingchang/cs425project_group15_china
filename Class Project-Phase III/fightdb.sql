CREATE TABLE Country(name varchar(100) primary key);

CREATE TABLE airline(code CHAR(4) primary key, name varchar(100), origin_country_name varchar(100),
constraint airline_count Foreign key (origin_country_name) references country(name));

CREATE TABLE flight(airline_code CHAR(4),flight_number CHAR(10), date date,
primary key (airline_code,flight_number , date),
constraint flight_airline Foreign key(airline_code) references airline(code));

CREATE TABLE class(name CHAR(4) primary key);

CREATE TABLE airport(airport_loc_id int primary key, name varchar(100), state varchar(50), country_name varchar(100),
constraint airport_count foreign key (country_name) references country(name));

CREATE TABLE customer(email varchar(100) primary key, name varchar(100),home_airport_loc_id int,
constraint cust_home foreign key (home_airport_loc_id) references airport(airport_loc_id));

CREATE TABLE airportDistance(airport_loc_id int, other_airport_loc_id int, distance real,
primary key(airport_loc_id, other_airport_loc_id),
constraint origin_loc foreign key (airport_loc_id) references airport(airport_loc_id),
constraint origin_loc_dest foreign key (other_airport_loc_id) references airport(airport_loc_id));

CREATE TABLE address(email varchar(100), address varchar(100), primary key (email, address),
Foreign key cust_add (email) references customer(email));

CREATE TABLE payment(credit_card varchar(20), email varchar(100), primary key (credit_card, email),
constraint cust_payment Foreign key cust_add (email) references customer(email));

CREATE TABLE flightLegs(airline_code CHAR(4), flight_number CHAR(10), date date, arrival_loc_id int,arrival_time varchar(10), dept_loc_id int, dept_time varchar(10),
primary key (airline_code, flight_number, date),
constraint fk_flights Foreign key (airline_code, flight_number, date) references flight(airline_code, flight_number, date),
constraint loc_arr foreign key (arrival_loc_id) references airport(airport_loc_id),
constraint loc_dept foreign key (dept_loc_id) references airport(airport_loc_id));

CREATE TABLE flightClasses(airline_code CHAR(4), flight_number CHAR(10), date date, class_name varchar(100), price real, capacity int,
primary key (airline_code, flight_number, date, class_name),
constraint fk_flightsclass Foreign key (airline_code, flight_number, date) references flight(airline_code, flight_number, date),
constraint fk_Class_name foreign key (class_name) references class(name));

CREATE TABLE CustomerBooking(customer_email varchar(100), airline_code CHAR(4), flight_number CHAR(10), date date,class_name varchar(100), credit_card varchar(20),
primary key (airline_code, flight_number, date, class_name, customer_email),
constraint fk_flightsclassbooking Foreign key (airline_code, flight_number, date, class_name) references flightClasses(airline_code, flight_number, date, class_name),
constraint fk_email foreign key (customer_email) references customer(email));