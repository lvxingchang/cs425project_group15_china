
from django.contrib.auth.models import Permission, User
from django.db import models


# Create your models here.
# Adding primary_key overrides the default primary key
class Flight(models.Model):
    user = models.ManyToManyField(User, default=1)
    name = models.CharField(max_length=100)
    leave_city = models.CharField(max_length=100, null=True)
    arrive_city = models.CharField(max_length=100, null=True)
    leave_airport = models.CharField(max_length=100, null=True)
    arrive_airport = models.CharField(max_length=100, null=True)
    leave_time = models.DateTimeField(null=True)
    arrive_time = models.DateTimeField(null=True)
    capacity = models.IntegerField(default=0, null=True)
    price = models.FloatField(default=0, null=True)
    book_sum = models.IntegerField(default=0, null=True)


class Country(models.Model)
    def __str__(self):
        return self.name
