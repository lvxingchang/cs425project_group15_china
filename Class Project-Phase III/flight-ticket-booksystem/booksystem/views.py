from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from .forms import PassengerInfoForm, UserForm
from .models import Flight
from .classes import IncomeMetric, Order
from django.contrib.auth.models import Permission, User
import datetime, pytz
from operator import attrgetter

ADMIN_ID = 1


def admin_finance(request):
    all_flights = Flight.objects.all()
    all_flights = sorted(all_flights, key=attrgetter('leave_time'))

    # Tag the input for each day of the flight with a different time [week, month, day]
    week_day_incomes = []
    month_day_incomes = []
    year_day_incomes = []

    # Use SET to store all weeks, months, and years
    week_set = set()
    month_set = set()
    year_set = set()
    for flight in all_flights:
        if flight.income > 0:  # Only revenue-generating flights are counted
            # Tag Last Week
            this_week = flight.leave_time.strftime('%W')  # datetime获取周
            week_day_incomes.append((this_week, flight.income))  # Add a tuple (Week, Income)
            week_set.add(this_week)

            this_month = flight.leave_time.strftime('%m')  # Datetime for month
            month_day_incomes.append((this_month, flight.income))  # Add a tuple (month, income)
            month_set.add(this_month)

            this_year = flight.leave_time.strftime('%Y')  # Datetime for years
            year_day_incomes.append((this_year, flight.income))  # Add a tuple (year, income)
            year_set.add(this_year)


    # # Store weekly income in the Week_Incomes List with Incomemetric type
    week_incomes = []
    for week in week_set:
        income = sum(x[1] for x in week_day_incomes if x[0] == week)  # The sum of income of the same week
        flight_sum = sum(1 for x in week_day_incomes if x[0] == week)  # Total number of flights in the same week
        week_income = IncomeMetric(week, flight_sum, income)  # Store the data in the Incomemetric class to facilitate Jinja syntax
        week_incomes.append(week_income)
    week_incomes = sorted(week_incomes, key=attrgetter('metric'))  # List Week_Incomes are arranged in ascending order weekly


    # Store the monthly income in the Month_Incomes List with IncomeMetric type
    month_incomes = []
    for month in month_set:
        income = sum(x[1] for x in month_day_incomes if x[0] == month)
        flight_sum = sum(1 for x in month_day_incomes if x[0] == month)
        month_income = IncomeMetric(month, flight_sum, income)
        month_incomes.append(month_income)
    month_incomes = sorted(month_incomes, key=attrgetter('metric'))  # 将List类型的 month_incomes 按月份升序排列


    # The annual income is stored in the Year_Incomes List with Incomemetric type
    year_incomes = []
    for year in year_set:
        income = sum(x[1] for x in year_day_incomes if x[0] == year)
        flight_sum = sum(1 for x in year_day_incomes if x[0] == year)
        year_income = IncomeMetric(year, flight_sum, income)
        year_incomes.append(year_income)
    year_incomes = sorted(year_incomes, key=attrgetter('metric'))  # Arrange the YEAR_INDS of the List type in ascending order of year


    passengers = User.objects.exclude(pk=1)  # Get rid of administrators
    order_set = set()
    for p in passengers:
        flights = Flight.objects.filter(user=p)
        for f in flights:
            route = f.leave_city + ' → ' + f.arrive_city
            order = Order(p.username, f.name, route, f.leave_time, f.price)
            order_set.add(order)


    context = {
        'week_incomes': week_incomes,
        'month_incomes': month_incomes,
        'year_incomes': year_incomes,
        'order_set': order_set
    }
    return context


def user_info(request):
    if request.user.is_authenticated():
        # If the user is an administrator, render company flight revenue statistics page admin_finance
        if request.user.id == ADMIN_ID:
            context = admin_finance(request)  # Gets the data to be passed to the front end
            return render(request, 'booksystem/admin_finance.html', context)
        # If the user is an ordinary user, the ticket information of the render user is user_info
        else:
            booked_flights = Flight.objects.filter(user=request.user)  # Filters the flight booked by the user from the booksystem_flight_user table
            context = {
                'booked_flights': booked_flights,
                'username': request.user.username,  # Navigation bar information updated
            }
            return render(request, 'booksystem/user_info.html', context)
    return render(request, 'booksystem/login.html')  # If the user is not logged in, the render login page



def index(request):
    return render(request, 'booksystem/index.html')


# csrf
@csrf_exempt
def book_ticket(request, flight_id):
    if not request.user.is_authenticated:  # Render logs in the page if not logged in
        return render(request, 'booksystem/login.html')
    else:
        flight = Flight.objects.get(pk=flight_id)
        booked_flights = Flight.objects.filter(user=request.user)  # Return QuerySet

        if flight in booked_flights:
            return render(request, 'booksystem/book_conflict.html')

        # After book_flight. HTML click to confirm, request is a POST method, passing no value, but a POST signal
        # Confirmed booking, Flight database changed

        # Verify that the same ticket can only be booked once
        if request.method == 'POST':
            if flight.capacity > 0:
                flight.book_sum += 1
                flight.capacity -= 1
                flight.income += flight.price
                flight.user.add(request.user)
                flight.save()
        # Pass the changed ticket information
        context = {
            'flight': flight,
            'username': request.user.username
        }
        return render(request, 'booksystem/book_flight.html', context)


# refund
def refund_ticket(request, flight_id):
    flight = Flight.objects.get(pk=flight_id)
    flight.book_sum -= 1
    flight.capacity += 1
    flight.income -= flight.price
    flight.user.remove(request.user)
    flight.save()
    return HttpResponseRedirect('/booksystem/user_info')


# Log out
def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'booksystem/login.html', context)


# The login
def login_user(request):
    if request.method == "POST":
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        user = authenticate(username=username, password=password)
        if user is not None:  # Login successful
            if user.is_active:  # Load the booking page
                login(request, user)
                context = {
                    'username': request.user.username
                }
                if user.id == ADMIN_ID:
                    context = admin_finance(request)  # Gets the data to be passed to the front end
                    return render(request, 'booksystem/admin_finance.html', context)
                else:
                    return render(request, 'booksystem/result.html', context)
            else:
                return render(request, 'booksystem/login.html', {'error_message': 'Your account has been disabled'})
        else:  # Login failed
            return render(request, 'booksystem/login.html', {'error_message': 'Invalid login'})
    return render(request, 'booksystem/login.html')


# registered
def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                context = {
                    'username': request.user.username
                }
                return render(request, 'booksystem/result.html', context)  # Successful registration goes directly to the Render Result page
    context = {
        "form": form,
    }
    return render(request, 'booksystem/register.html', context)


# Search results page
def result(request):
    if request.method == 'POST':
        form = PassengerInfoForm(request.POST)  # Bind data to the form
        if form.is_valid():
            passenger_lcity = form.cleaned_data.get('leave_city')
            passenger_acity = form.cleaned_data.get('arrive_city')
            passenger_ldate = form.cleaned_data.get('leave_date')
            # print(type(passenger_ldate))


            # china_tz = pytz.timezone('Asia/Shanghai')
            # passenger_ltime = datetime.datetime(
            #     year=passenger_ldate.year,
            #     month=passenger_ldate.month,
            #     day=passenger_ldate.day,
            #     hour=0, minute=0, second=0,
            #     tzinfo=china_tz
            # )


            passenger_ltime = datetime.datetime.combine(passenger_ldate, datetime.time())
            print(passenger_ltime)


            all_flights = Flight.objects.filter(leave_city=passenger_lcity, arrive_city=passenger_acity)
            usable_flights = []
            for flight in all_flights:  # off-set aware
                flight.leave_time = flight.leave_time.replace(tzinfo=None)
                if flight.leave_time.date() == passenger_ltime.date():
                    usable_flights.append(flight)

            # Sort by different keys
            usable_flights_by_ltime = sorted(usable_flights, key=attrgetter('leave_time'))  # The departure time is from morning till night
            usable_flights_by_atime = sorted(usable_flights, key=attrgetter('arrive_time'))
            usable_flights_by_price = sorted(usable_flights, key=attrgetter('price'))  # Prices vary from low to high

            # Conversion time format
            time_format = '%H:%M'
            # for flight in usable_flights_by_ltime:
            #     flight.leave_time = flight.leave_time.strftime(time_format)  # 转成了str
            #     flight.arrive_time = flight.arrive_time.strftime(time_format)
            #
            # for flight in usable_flights_by_atime:
            #     flight.leave_time = flight.leave_time.strftime(time_format)  # 转成了str
            #     flight.arrive_time = flight.arrive_time.strftime(time_format)

            # Although only one list has been converted, In fact, all of them have been converted
            for flight in usable_flights_by_price:
                flight.leave_time = flight.leave_time.strftime(time_format)  # Into the STR
                flight.arrive_time = flight.arrive_time.strftime(time_format)

            # Determines whether search_head, search_failure are displayed
            dis_search_head = 'block'
            dis_search_failure = 'none'
            if len(usable_flights_by_price) == 0:
                dis_search_head = 'none'
                dis_search_failure = 'block'
            context = {
                # Search for multi-box data
                'leave_city': passenger_lcity,
                'arrive_city': passenger_acity,
                'leave_date': str(passenger_ldate),
                # The search results
                'usable_flights_by_ltime': usable_flights_by_ltime,
                'usable_flights_by_atime': usable_flights_by_atime,
                'usable_flights_by_price': usable_flights_by_price,
                # tag
                'dis_search_head': dis_search_head,
                'dis_search_failure': dis_search_failure
            }
            if request.user.is_authenticated:
                context['username'] = request.user.username
            return render(request, 'booksystem/result.html', context)  # If you add/to the root directory, it will be the root directory. The URL is wrong
        else:
            return render(request, 'booksystem/index.html')  # The form submitted on the Index screen is invalid, so it stays on the Index screen
    else:
        context = {
            'dis_search_head': 'none',
            'dis_search_failure': 'none'
        }
    return render(request, 'booksystem/result.html', context)
