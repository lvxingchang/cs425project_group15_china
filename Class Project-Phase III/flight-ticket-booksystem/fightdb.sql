/*
 Navicat Premium Data Transfer

 Source Server         : MyDj
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : fightdb

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 05/05/2021 20:46:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq`(`group_id`, `permission_id`) USING BTREE,
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq`(`content_type_id`, `codename`) USING BTREE,
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO `auth_permission` VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO `auth_permission` VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO `auth_permission` VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO `auth_permission` VALUES (5, 'Can add permission', 2, 'add_permission');
INSERT INTO `auth_permission` VALUES (6, 'Can change permission', 2, 'change_permission');
INSERT INTO `auth_permission` VALUES (7, 'Can delete permission', 2, 'delete_permission');
INSERT INTO `auth_permission` VALUES (8, 'Can view permission', 2, 'view_permission');
INSERT INTO `auth_permission` VALUES (9, 'Can add group', 3, 'add_group');
INSERT INTO `auth_permission` VALUES (10, 'Can change group', 3, 'change_group');
INSERT INTO `auth_permission` VALUES (11, 'Can delete group', 3, 'delete_group');
INSERT INTO `auth_permission` VALUES (12, 'Can view group', 3, 'view_group');
INSERT INTO `auth_permission` VALUES (13, 'Can add user', 4, 'add_user');
INSERT INTO `auth_permission` VALUES (14, 'Can change user', 4, 'change_user');
INSERT INTO `auth_permission` VALUES (15, 'Can delete user', 4, 'delete_user');
INSERT INTO `auth_permission` VALUES (16, 'Can view user', 4, 'view_user');
INSERT INTO `auth_permission` VALUES (17, 'Can add content type', 5, 'add_contenttype');
INSERT INTO `auth_permission` VALUES (18, 'Can change content type', 5, 'change_contenttype');
INSERT INTO `auth_permission` VALUES (19, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO `auth_permission` VALUES (20, 'Can view content type', 5, 'view_contenttype');
INSERT INTO `auth_permission` VALUES (21, 'Can add session', 6, 'add_session');
INSERT INTO `auth_permission` VALUES (22, 'Can change session', 6, 'change_session');
INSERT INTO `auth_permission` VALUES (23, 'Can delete session', 6, 'delete_session');
INSERT INTO `auth_permission` VALUES (24, 'Can view session', 6, 'view_session');
INSERT INTO `auth_permission` VALUES (25, 'Can add flight', 7, 'add_flight');
INSERT INTO `auth_permission` VALUES (26, 'Can change flight', 7, 'change_flight');
INSERT INTO `auth_permission` VALUES (27, 'Can delete flight', 7, 'delete_flight');
INSERT INTO `auth_permission` VALUES (28, 'Can view flight', 7, 'view_flight');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_login` datetime(6) NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `first_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_groups_user_id_group_id_94350c0c_uniq`(`user_id`, `group_id`) USING BTREE,
  INDEX `auth_user_groups_group_id_97559544_fk_auth_group_id`(`group_id`) USING BTREE,
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq`(`user_id`, `permission_id`) USING BTREE,
  INDEX `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for booksystem_flight
-- ----------------------------
DROP TABLE IF EXISTS `booksystem_flight`;
CREATE TABLE `booksystem_flight`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `leave_city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `arrive_city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `leave_airport` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `arrive_airport` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `leave_time` datetime(6) NULL DEFAULT NULL,
  `arrive_time` datetime(6) NULL DEFAULT NULL,
  `capacity` int(11) NULL DEFAULT NULL,
  `price` double NULL DEFAULT NULL,
  `book_sum` int(11) NULL DEFAULT NULL,
  `income` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of booksystem_flight
-- ----------------------------
INSERT INTO `booksystem_flight` VALUES (1, '南方航空CZ3123', '长沙', '北京', '黄花国际机场T2', '首都国际机场T2', '2017-04-02 07:30:00.000000', '2017-04-02 09:45:00.000000', 100, 890, 0, 0);
INSERT INTO `booksystem_flight` VALUES (2, '中国国航CA1350', '长沙', '北京', '黄花国际机场T2', '首都国际机场T3', '2017-04-02 08:35:00.000000', '2017-04-02 10:55:00.000000', 100, 1410, 0, 0);
INSERT INTO `booksystem_flight` VALUES (3, '厦门航空MF8147', '长沙', '北京', '黄花国际机场T2', '首都国际机场T2', '2017-04-02 18:50:00.000000', '2017-04-02 21:05:00.000000', 100, 1080, 0, 0);
INSERT INTO `booksystem_flight` VALUES (4, '海南航空HU7636', '长沙', '北京', '黄花国际机场T2', '首都国际机场T1', '2017-04-02 11:20:00.000000', '2017-04-02 13:40:00.000000', 99, 910, 1, 910);
INSERT INTO `booksystem_flight` VALUES (5, '海南航空HU7815', '长沙', '上海', '黄花国际机场T2', '浦东国际机场T2', '2017-04-02 07:55:00.000000', '2017-04-02 09:55:00.000000', 99, 480, 1, 480);
INSERT INTO `booksystem_flight` VALUES (6, '东方航空MU5368', '长沙', '上海', '黄花国际机场T2', '浦东国际机场T1', '2017-04-02 08:10:00.000000', '2017-04-02 09:55:00.000000', 100, 590, 0, 0);
INSERT INTO `booksystem_flight` VALUES (7, '南方航空CZ9646', '长沙', '上海', '黄花国际机场T2', '虹桥国际机场T2', '2017-04-02 10:00:00.000000', '2017-04-02 11:45:00.000000', 100, 870, 0, 0);
INSERT INTO `booksystem_flight` VALUES (8, '吉祥航空HO1124', '长沙', '上海', '黄花国际机场T2', '虹桥国际机场T2', '2017-04-02 10:05:00.000000', '2017-04-02 11:50:00.000000', 100, 680, 0, 0);
INSERT INTO `booksystem_flight` VALUES (9, '南方航空CZ332', '长沙', '广州', '黄花国际机场T2', '白云国际机场', '2017-04-02 08:15:00.000000', '2017-04-02 09:35:00.000000', 100, 370, 0, 0);
INSERT INTO `booksystem_flight` VALUES (10, '南方航空CZ3387', '长沙', '广州', '黄花国际机场T2', '白云国际机场', '2017-04-02 09:15:00.000000', '2017-04-02 10:35:00.000000', 100, 490, 0, 0);
INSERT INTO `booksystem_flight` VALUES (11, '四川航空3U8998', '长沙', '成都', '黄花国际机场T2', '双流国际机场T1', '2017-04-02 14:25:00.000000', '2017-04-02 16:25:00.000000', 100, 1110, 0, 0);
INSERT INTO `booksystem_flight` VALUES (12, '奥凯航空BK2779', '长沙', '成都', '黄花国际机场T2', '双流国际机场T2', '2017-04-02 19:00:00.000000', '2017-04-02 20:50:00.000000', 100, 983, 0, 0);
INSERT INTO `booksystem_flight` VALUES (13, '南方航空CZ3721', '长沙', '西安', '黄花国际机场T2', '咸阳国际机场T3', '2017-04-02 08:05:00.000000', '2017-04-02 09:50:00.000000', 100, 470, 0, 0);
INSERT INTO `booksystem_flight` VALUES (14, '东方航空MU2384', '长沙', '西安', '黄花国际机场T2', '咸阳国际机场T3', '2017-04-02 20:20:00.000000', '2017-04-02 22:15:00.000000', 100, 420, 0, 0);
INSERT INTO `booksystem_flight` VALUES (15, '厦门航空MF8503', '长沙', '厦门', '黄花国际机场T2', '高崎国际机场T3', '2017-04-02 07:25:00.000000', '2017-04-02 08:55:00.000000', 100, 1050, 0, 0);
INSERT INTO `booksystem_flight` VALUES (16, '山东航空SC4786', '长沙', '厦门', '黄花国际机场T2', '高崎国际机场T4', '2017-04-02 09:30:00.000000', '2017-04-02 10:55:00.000000', 100, 438, 0, 0);
INSERT INTO `booksystem_flight` VALUES (18, '南方航空CZ3123', '长沙', '北京', '黄花国际机场T2', '首都国际机场T2', '2017-04-07 07:30:00.000000', '2017-04-07 09:45:00.000000', 100, 1030, 0, 0);
INSERT INTO `booksystem_flight` VALUES (19, '厦门航空MF8147', '长沙', '北京', '黄花国际机场T2', '首都国际机场T2', '2017-04-07 18:50:00.000000', '2017-04-07 21:05:00.000000', 100, 895, 0, 0);
INSERT INTO `booksystem_flight` VALUES (20, '海南航空HU7815', '长沙', '上海', '黄花国际机场T2', '浦东国际机场T2', '2017-05-04 07:55:00.000000', '2017-05-04 09:55:00.000000', 99, 480, 1, 480);

-- ----------------------------
-- Table structure for booksystem_flight_user
-- ----------------------------
DROP TABLE IF EXISTS `booksystem_flight_user`;
CREATE TABLE `booksystem_flight_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flight_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `booksystem_flight_user_flight_id_user_id_a90373fe_uniq`(`flight_id`, `user_id`) USING BTREE,
  INDEX `booksystem_flight_user_user_id_2c47b21d_fk_auth_user_id`(`user_id`) USING BTREE,
  CONSTRAINT `booksystem_flight_us_flight_id_3b27f016_fk_booksyste` FOREIGN KEY (`flight_id`) REFERENCES `booksystem_flight` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `booksystem_flight_user_user_id_2c47b21d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `object_repr` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content_type_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `django_admin_log_content_type_id_c4bce8eb_fk_django_co`(`content_type_id`) USING BTREE,
  INDEX `django_admin_log_user_id_c564eba6_fk_auth_user_id`(`user_id`) USING BTREE,
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq`(`app_label`, `model`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES (1, 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES (3, 'auth', 'group');
INSERT INTO `django_content_type` VALUES (2, 'auth', 'permission');
INSERT INTO `django_content_type` VALUES (4, 'auth', 'user');
INSERT INTO `django_content_type` VALUES (7, 'booksystem', 'flight');
INSERT INTO `django_content_type` VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES (6, 'sessions', 'session');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES (1, 'contenttypes', '0001_initial', '2021-05-05 20:40:41.706950');
INSERT INTO `django_migrations` VALUES (2, 'auth', '0001_initial', '2021-05-05 20:40:41.816129');
INSERT INTO `django_migrations` VALUES (3, 'admin', '0001_initial', '2021-05-05 20:40:41.846568');
INSERT INTO `django_migrations` VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2021-05-05 20:40:41.850457');
INSERT INTO `django_migrations` VALUES (5, 'admin', '0003_logentry_add_action_flag_choices', '2021-05-05 20:40:41.853724');
INSERT INTO `django_migrations` VALUES (6, 'contenttypes', '0002_remove_content_type_name', '2021-05-05 20:40:41.875051');
INSERT INTO `django_migrations` VALUES (7, 'auth', '0002_alter_permission_name_max_length', '2021-05-05 20:40:41.888900');
INSERT INTO `django_migrations` VALUES (8, 'auth', '0003_alter_user_email_max_length', '2021-05-05 20:40:41.904484');
INSERT INTO `django_migrations` VALUES (9, 'auth', '0004_alter_user_username_opts', '2021-05-05 20:40:41.908176');
INSERT INTO `django_migrations` VALUES (10, 'auth', '0005_alter_user_last_login_null', '2021-05-05 20:40:41.922257');
INSERT INTO `django_migrations` VALUES (11, 'auth', '0006_require_contenttypes_0002', '2021-05-05 20:40:41.923573');
INSERT INTO `django_migrations` VALUES (12, 'auth', '0007_alter_validators_add_error_messages', '2021-05-05 20:40:41.927877');
INSERT INTO `django_migrations` VALUES (13, 'auth', '0008_alter_user_username_max_length', '2021-05-05 20:40:41.943063');
INSERT INTO `django_migrations` VALUES (14, 'auth', '0009_alter_user_last_name_max_length', '2021-05-05 20:40:41.959673');
INSERT INTO `django_migrations` VALUES (15, 'auth', '0010_alter_group_name_max_length', '2021-05-05 20:40:41.973234');
INSERT INTO `django_migrations` VALUES (16, 'auth', '0011_update_proxy_permissions', '2021-05-05 20:40:41.977503');
INSERT INTO `django_migrations` VALUES (17, 'auth', '0012_alter_user_first_name_max_length', '2021-05-05 20:40:41.992525');
INSERT INTO `django_migrations` VALUES (18, 'booksystem', '0001_initial', '2021-05-05 20:40:42.018260');
INSERT INTO `django_migrations` VALUES (19, 'booksystem', '0002_auto_20210402_2346', '2021-05-05 20:40:42.052164');
INSERT INTO `django_migrations` VALUES (20, 'booksystem', '0003_auto_20210403_1941', '2021-05-05 20:40:42.094946');
INSERT INTO `django_migrations` VALUES (21, 'booksystem', '0004_auto_20210404_1205', '2021-05-05 20:40:42.100346');
INSERT INTO `django_migrations` VALUES (22, 'booksystem', '0005_auto_20210404_1208', '2021-05-05 20:40:42.104391');
INSERT INTO `django_migrations` VALUES (23, 'sessions', '0001_initial', '2021-05-05 20:40:42.112980');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session`  (
  `session_key` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `session_data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`) USING BTREE,
  INDEX `django_session_expire_date_a5c62663`(`expire_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
