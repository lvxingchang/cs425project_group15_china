Name:Xingchang Lyu
CWID:A20490957
Group:15

# I. Project introduction

## 1.1 Title Requirements

The basic functions required are as follows:

- Management of basic information of flights
- Flight information management
- Management of passenger booking, canceling reservations, paying for and withdrawing tickets
- Check flight information, flight reservation, passenger information, and calculate the flight occupancy rate.

## 1.2 Development environment

- Language: Python 3.8
- Framework: Django 
- Front-end design: HTML, CSS, JavaScript
- Development environment: PyCharm

# II. Needs analysis

## 2.1 Specific needs

The goal is to build an airline flflight booking application. Passengers can inquire about the company's flight situation through the online booking platform, and arrange flights for passengers by entering the information system such as departure place, destination, departure time, etc. Passengers can choose the flight according to their own needs and the departure time and ticket price. After the booking is successful, the system generates the order information for the passengers. Passengers can view their booking information on the personal information page, and can make a refund request to the system. The system calculates the handling fee according to the specific situation and then handles the corresponding refund process.

## 2.2 Functional analysis

### 2.2.1 User interface

- Inquiry: Users query flight information
- Sort: Users sort and filter query results according to their own needs.
- Booking: Enter and record passenger reservation information and update database information for user booking needs.
- Ticket refund: process the user's ticket refund needs and update the database.
- View personal information: users view their personal ticket information
- Help: Provide help documents for system use
- Quit: Close the current page

### 2.2.2 Administrator interface

- Flight information management: flight information can be added, deleted, modified and checked.
- Passenger information management: passenger information can be added, deleted and checked.
- Help: Provide help documents for system use
- Quit: Close the current page

## 2.3 System main function diagram

![iShot2021-05-15 22.30.40](https://tva1.sinaimg.cn/large/008i3skNgy1gqjhm4m7g9j30fu095my2.jpg)



### 2.4 System Data Flow Diagram

![iShot2021-05-15 22.30.51](https://tva1.sinaimg.cn/large/008i3skNgy1gqjhll0fujj30gk0au751.jpg)



## III. Logical design

### 3.1 ER diagram

![IMG_299A81D27884-1](https://tva1.sinaimg.cn/large/008i3skNgy1gqjhnhg670j31fz0u0dqh.jpg)



**Create tables from Entities along with attributes:**

1. Airport(**<u>idata_loc_id</u>**, name, state)

2. Country(**<u>name</u>**)

3. flight(**<u>flight_number</u>, <u>date</u>**)

4. class(**<u>name</u>**)

5. customer(**<u>email</u>**, name)

6. address(**<u>address</u>**)

7. payent(**<u>credit_card</u>**)

8. airline(**<u>code</u>**, name)

**Solve relationships by adding a reference from other tables:**

**1.** The distance between two airports is to be stored as below:

airportDistance(**<u>airport_loc_id, other_airport_loc_id</u>**, distance)

2. Customer has home at one airport location, thus customer will have a foreign key to ts airport location as below:

customer(**<u>email</u>**, name, *home_airport_loc_id*)

3. Airport is located in one country and a country may have located many airports in it.

airport(airport_loc_id, name, state, *country_name)*

4. Flight is operated by airline and flight number is unique within the <date, airline code>. Thus flight will be completed as below to have its primary key

flight(<u>airline_code, flight_number, date</u>)

5. A Flight has many classes and different cost for each class. It can be stored as below:

flightClasses(<u>airline_code, flight_number, date, class_name,price</u>)

6. A Flight has different classes capacity, which will be stored in same flightClasses table as below:

flightClasses(<u>airline_code, flight_number, date, class_name, price</u>, capacity)

7. Flight arrives and departs from many airports with a time, which is needed to be stored at flight table as below along with arrival time the:

flightLegs(<u>airline_code, flight_number, date, arrival_loc_id</u>, arrival_time, dept_loc_id, dept_time)

8. Airline origins from one country which is to be stored as below:

airline(**<u>code</u>**, name, *origin_country_name*)

9. Customer book a flight , having a class and having a payment as below:

CustomerBooking(<u>customer_email, class_name, credit_card, airline_code, flight_number_date</u>)

10. Customer has many addresses which will be stored in address as below:

address(**<u>email, address</u>**)

**Final list of tables:**

Primary key is bold and underlined, foreign key is mentioned explicitly along with marked as italic

1. Country(**<u>name</u>**)

2. airline(**<u>code</u>**, name, *origin_country_name*)

Foreign key: *origin*_country_name from country

3. flight(<u>airline_code ,flight_number, date</u>)

Foreign key: *airline_code from airline*

4. class(**<u>name</u>**)

*5.* airport(**<u>airport_loc_id</u>,** name, state, *country_name)*

Foreign key: *country_name from country*

6. customer(**<u>email</u>**, name, *home_airport_loc_id*)

Foreign key: *home_airport_loc_id from airport*

7. **airportDistance(<u>airport_loc_id, other_airport_loc_id</u>, distance)

Foreign key: *airport_loc_id, other_airport_loc_id both from airport*

8. address(<u>email, address</u>)

Foreign key: *email from customer*

9. payment(<u>credit_card, email</u>)

Foreign key: *email from customer*

10. flightLegs(**<u>airline_code, flight_number, date, arrival_loc_id</u>**, arrival_time, dept_loc_id, dept_time)

Foreign key: *( airline_code, flight_number, date) from flight*

Foreign key: *arrival_loc_id from airport*

Foreign key: *dept_loc_id from airport*

11. flightClasses(**<u>airline_code, flight_number, date,class_name</u>**, price, capacity)

Foreign key: *( airline_code, flight_number, date) from flight*

Foreign key: *class_name from class*

12. CustomerBooking(**c<u>ustomer_email, class_name, credit_card, airline_code, flight_number_date</u>**)

Foreign key: *( airline_code, flight_number, date, class_name) from flightClasses*

Foreign key: *email from customer*

![iShot2021-05-15 16.19.56](https://tva1.sinaimg.cn/large/008i3skNgy1gqjhp43gauj30vs0ottcp.jpg)

```SQL
CREATE TABLE Country(name varchar(100) primary key); 

CREATE TABLE airline(code CHAR(4) primary key, name varchar(100), origin_country_name varchar(100),
constraint airline_count Foreign key (origin_country_name) references country(name));

CREATE TABLE flight(airline_code CHAR(4),flight_number CHAR(10), date date,
primary key (airline_code,flight_number , date),
constraint flight_airline Foreign key(airline_code) references airline(code));

CREATE TABLE class(name CHAR(4) primary key);

CREATE TABLE airport(airport_loc_id int primary key, name varchar(100), state varchar(50), country_name varchar(100),
constraint airport_count foreign key (country_name) references country(name));

CREATE TABLE customer(email varchar(100) primary key, name varchar(100),home_airport_loc_id int,
constraint cust_home foreign key (home_airport_loc_id) references airport(airport_loc_id));

CREATE TABLE airportDistance(airport_loc_id int, other_airport_loc_id int, distance real,
primary key(airport_loc_id, other_airport_loc_id),
constraint origin_loc foreign key (airport_loc_id) references airport(airport_loc_id),
constraint origin_loc_dest foreign key (other_airport_loc_id) references airport(airport_loc_id));

CREATE TABLE address(email varchar(100), address varchar(100), primary key (email, address),
Foreign key cust_add (email) references customer(email));

CREATE TABLE payment(credit_card varchar(20), email varchar(100), primary key (credit_card, email),
constraint cust_payment Foreign key cust_add (email) references customer(email));

CREATE TABLE flightLegs(airline_code CHAR(4), flight_number CHAR(10), date date, arrival_loc_id int,arrival_time varchar(10), dept_loc_id int, dept_time varchar(10),
primary key (airline_code, flight_number, date),
constraint fk_flights Foreign key (airline_code, flight_number, date) references flight(airline_code, flight_number, date),
constraint loc_arr foreign key (arrival_loc_id) references airport(airport_loc_id),
constraint loc_dept foreign key (dept_loc_id) references airport(airport_loc_id));

CREATE TABLE flightClasses(airline_code CHAR(4), flight_number CHAR(10), date date, class_name varchar(100), price real, capacity int,
primary key (airline_code, flight_number, date, class_name),
constraint fk_flightsclass Foreign key (airline_code, flight_number, date) references flight(airline_code, flight_number, date),
constraint fk_Class_name foreign key (class_name) references class(name));

CREATE TABLE CustomerBooking(customer_email varchar(100), airline_code CHAR(4), flight_number CHAR(10), date date,class_name varchar(100), credit_card varchar(20),
primary key (airline_code, flight_number, date, class_name, customer_email),
constraint fk_flightsclassbooking Foreign key (airline_code, flight_number, date, class_name) references flightClasses(airline_code, flight_number, date, class_name),
constraint fk_email foreign key (customer_email) references customer(email));
```



# IV. Functional design

The Python + Django + Mysql design method adopted by this system, and the background function is written in the views.py file.

![iShot2021-05-16 00.13.04](https://tva1.sinaimg.cn/large/008i3skNgy1gqjkfcxpukj30fg0f2myd.jpg)

## 4.1 User module

### 4.1.1 Ticket booking module

In the process of booking the user, first determine whether the user is logged in or not, load the login page; if the user has logged in, enter flight.id through the flight selected by the front-end user, and then judge whether the user has booked the flight, feedback the conflict information. If not, book Ticket success, database update, display ticket booking success page.

```python
# csrf
@csrf_exempt
def book_ticket(request, flight_id):
    if not request.user.is_authenticated:  # Render logs in the page if not logged in
        return render(request, 'booksystem/login.html')
    else:
        flight = Flight.objects.get(pk=flight_id)
        booked_flights = Flight.objects.filter(user=request.user)  # Return QuerySet

        if flight in booked_flights:
            return render(request, 'booksystem/book_conflict.html')

        # After book_flight. HTML click to confirm, request is a POST method, passing no value, but a POST signal
        # Confirmed booking, Flight database changed

        # Verify that the same ticket can only be booked once
        if request.method == 'POST':
            if flight.capacity > 0:
                flight.book_sum += 1
                flight.capacity -= 1
                flight.income += flight.price
                flight.user.add(request.user)
                flight.save()
        # Pass the changed ticket information
        context = {
            'flight': flight,
            'username': request.user.username
        }
        return render(request, 'booksystem/book_flight.html', context)
```

#### 4.1.2 Query module

The front-end form receives the origin, destination and departure time incoming by the user, and then finds the eligible flight in the flight database, which is divided into two steps:

1. Find flights with the same origin and destination;
2. Find flights with the same departure date as the departure date of passengers.

In order to give users a good experience, the flight information that meets the conditions is arranged in ascending order according to different key values (**take-off time, landing time, ticket price**).

```python
# Search results page
def result(request):
    if request.method == 'POST':
        form = PassengerInfoForm(request.POST)  # Bind data to the form
        if form.is_valid():
            passenger_lcity = form.cleaned_data.get('leave_city')
            passenger_acity = form.cleaned_data.get('arrive_city')
            passenger_ldate = form.cleaned_data.get('leave_date')
            # print(type(passenger_ldate))


            # china_tz = pytz.timezone('Asia/Shanghai')
            # passenger_ltime = datetime.datetime(
            #     year=passenger_ldate.year,
            #     month=passenger_ldate.month,
            #     day=passenger_ldate.day,
            #     hour=0, minute=0, second=0,
            #     tzinfo=china_tz
            # )


            passenger_ltime = datetime.datetime.combine(passenger_ldate, datetime.time())
            print(passenger_ltime)


            all_flights = Flight.objects.filter(leave_city=passenger_lcity, arrive_city=passenger_acity)
            usable_flights = []
            for flight in all_flights:  # off-set aware
                flight.leave_time = flight.leave_time.replace(tzinfo=None)
                if flight.leave_time.date() == passenger_ltime.date():
                    usable_flights.append(flight)

            # Sort by different keys
            usable_flights_by_ltime = sorted(usable_flights, key=attrgetter('leave_time'))  # The departure time is from morning till night
            usable_flights_by_atime = sorted(usable_flights, key=attrgetter('arrive_time'))
            usable_flights_by_price = sorted(usable_flights, key=attrgetter('price'))  # Prices vary from low to high

            # Conversion time format
            time_format = '%H:%M'
            # for flight in usable_flights_by_ltime:
            #     flight.leave_time = flight.leave_time.strftime(time_format)  # 转成了str
            #     flight.arrive_time = flight.arrive_time.strftime(time_format)
            #
            # for flight in usable_flights_by_atime:
            #     flight.leave_time = flight.leave_time.strftime(time_format)  # 转成了str
            #     flight.arrive_time = flight.arrive_time.strftime(time_format)

            # Although only one list has been converted, In fact, all of them have been converted
            for flight in usable_flights_by_price:
                flight.leave_time = flight.leave_time.strftime(time_format)  # Into the STR
                flight.arrive_time = flight.arrive_time.strftime(time_format)

            # Determines whether search_head, search_failure are displayed
            dis_search_head = 'block'
            dis_search_failure = 'none'
            if len(usable_flights_by_price) == 0:
                dis_search_head = 'none'
                dis_search_failure = 'block'
            context = {
                # Search for multi-box data
                'leave_city': passenger_lcity,
                'arrive_city': passenger_acity,
                'leave_date': str(passenger_ldate),
                # The search results
                'usable_flights_by_ltime': usable_flights_by_ltime,
                'usable_flights_by_atime': usable_flights_by_atime,
                'usable_flights_by_price': usable_flights_by_price,
                # tag
                'dis_search_head': dis_search_head,
                'dis_search_failure': dis_search_failure
            }
            if request.user.is_authenticated:
                context['username'] = request.user.username
            return render(request, 'booksystem/result.html', context)  # If you add/to the root directory, it will be the root directory. The URL is wrong
        else:
            return render(request, 'booksystem/index.html')  # The form submitted on the Index screen is invalid, so it stays on the Index screen
    else:
        context = {
            'dis_search_head': 'none',
            'dis_search_failure': 'none'
        }
    return render(request, 'booksystem/result.html', context)
```

#### 4.1.3 Ticket refund module

When refunding a ticket, you need to update the database, update the (capacity, book_sum, income) field of the flight, and delete the order in the booksystem_flight_user table.

```
# refund
def refund_ticket(request, flight_id):
    flight = Flight.objects.get(pk=flight_id)
    flight.book_sum -= 1
    flight.capacity += 1
    flight.income -= flight.price
    flight.user.remove(request.user)
    flight.save()
    return HttpResponseRedirect('/booksystem/user_info')
```

#### 4.1.4 Personal Information Module

Because the administrator and the user share a login window, when displaying user information, it is necessary to determine the identity of the logged-in user. If the logged-in user is an administrator, the management page will be loaded, and if it is an ordinary user, the user's personal order page will be loaded

```
def user_info(request):
    if request.user.is_authenticated():
        # If the user is an administrator, render company flight revenue statistics page admin_finance
        if request.user.id == ADMIN_ID:
            context = admin_finance(request)  # Gets the data to be passed to the front end
            return render(request, 'booksystem/admin_finance.html', context)
        # If the user is an ordinary user, the ticket information of the render user is user_info
        else:
            booked_flights = Flight.objects.filter(user=request.user)  # Filters the flight booked by the user from the booksystem_flight_user table
            context = {
                'booked_flights': booked_flights,
                'username': request.user.username,  # Navigation bar information updated
            }
            return render(request, 'booksystem/user_info.html', context)
    return render(request, 'booksystem/login.html')  # If the user is not logged in, the render login page
```

### 4.2 Administrator module

#### 4.2.1 Flight information management

The flight object inherits models.Model, and flight information management is implemented in the Django default background management interface.

```

from django.contrib.auth.models import Permission, User
from django.db import models


# Create your models here.
# Adding primary_key overrides the default primary key
class Flight(models.Model):
    user = models.ManyToManyField(User, default=1)  
    name = models.CharField(max_length=100)  
    leave_city = models.CharField(max_length=100, null=True) 
    arrive_city = models.CharField(max_length=100, null=True)  
    leave_airport = models.CharField(max_length=100, null=True)  
    arrive_airport = models.CharField(max_length=100, null=True)  
    leave_time = models.DateTimeField(null=True)  
    arrive_time = models.DateTimeField(null=True)
    capacity = models.IntegerField(default=0, null=True)  
    price = models.FloatField(default=0, null=True)  
    book_sum = models.IntegerField(default=0, null=True)  
```

When using Django's default background management, because there are many fields in Flight, and the user object cannot be entered from the background, the user is out of Django's default form management, because Flight and U The relationship between ser is many to many, so the book_system table established by Django does not have user fields, which is solved in the following methods.

```
# 自定义Flight对象的输入信息
class FlightForm(forms.ModelForm):
    class Meta:
        model = Flight
        exclude = ['user']  # user信息不能从后台输入
```

#### 4.2.2 Passenger information management

The passenger inherits the django.contrib.auth.User class, and we only need to customize the objects that the user form needs to enter, and other default generated fields need not be considered.

```
# Fields that the user needs to enter
class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
```

#### 4.2.3 Aviation financial statistics

Count the airline's weekly, monthly and annual revenues and display all order information.

```
def admin_finance(request):
    all_flights = Flight.objects.all()
    all_flights = sorted(all_flights, key=attrgetter('leave_time'))

    # Tag the input for each day of the flight with a different time [week, month, day]
    week_day_incomes = []
    month_day_incomes = []
    year_day_incomes = []

    # Use SET to store all weeks, months, and years
    week_set = set()
    month_set = set()
    year_set = set()
    for flight in all_flights:
        if flight.income > 0:  # Only revenue-generating flights are counted
            # Tag Last Week
            this_week = flight.leave_time.strftime('%W')  # datetime获取周
            week_day_incomes.append((this_week, flight.income))  # Add a tuple (Week, Income)
            week_set.add(this_week)

            this_month = flight.leave_time.strftime('%m')  # Datetime for month
            month_day_incomes.append((this_month, flight.income))  # Add a tuple (month, income)
            month_set.add(this_month)

            this_year = flight.leave_time.strftime('%Y')  # Datetime for years
            year_day_incomes.append((this_year, flight.income))  # Add a tuple (year, income)
            year_set.add(this_year)


    # # Store weekly income in the Week_Incomes List with Incomemetric type
    week_incomes = []
    for week in week_set:
        income = sum(x[1] for x in week_day_incomes if x[0] == week)  # The sum of income of the same week
        flight_sum = sum(1 for x in week_day_incomes if x[0] == week)  # Total number of flights in the same week
        week_income = IncomeMetric(week, flight_sum, income)  # Store the data in the Incomemetric class to facilitate Jinja syntax
        week_incomes.append(week_income)
    week_incomes = sorted(week_incomes, key=attrgetter('metric'))  # List Week_Incomes are arranged in ascending order weekly


    # Store the monthly income in the Month_Incomes List with IncomeMetric type
    month_incomes = []
    for month in month_set:
        income = sum(x[1] for x in month_day_incomes if x[0] == month)
        flight_sum = sum(1 for x in month_day_incomes if x[0] == month)
        month_income = IncomeMetric(month, flight_sum, income)
        month_incomes.append(month_income)
    month_incomes = sorted(month_incomes, key=attrgetter('metric'))  # 将List类型的 month_incomes 按月份升序排列


    # The annual income is stored in the Year_Incomes List with Incomemetric type
    year_incomes = []
    for year in year_set:
        income = sum(x[1] for x in year_day_incomes if x[0] == year)
        flight_sum = sum(1 for x in year_day_incomes if x[0] == year)
        year_income = IncomeMetric(year, flight_sum, income)
        year_incomes.append(year_income)
    year_incomes = sorted(year_incomes, key=attrgetter('metric'))  # Arrange the YEAR_INDS of the List type in ascending order of year


    passengers = User.objects.exclude(pk=1)  # Get rid of administrators
    order_set = set()
    for p in passengers:
        flights = Flight.objects.filter(user=p)
        for f in flights:
            route = f.leave_city + ' → ' + f.arrive_city
            order = Order(p.username, f.name, route, f.leave_time, f.price)
            order_set.add(order)


    context = {
        'week_incomes': week_incomes,
        'month_incomes': month_incomes,
        'year_incomes': year_incomes,
        'order_set': order_set
    }
    return context

```

## V. Interface design

### 5.1 Welcome interface

![iShot2021-05-15 22.40.33](https://tva1.sinaimg.cn/large/008i3skNgy1gqjhqoufpgj314n0mib29.jpg)

Draw up a trip (Changsha → Shanghai 2017/4/2)

### 5.2 Query interface

After the user Let's Go, load the query results page.

![iShot2021-05-15 22.54.58](https://tva1.sinaimg.cn/large/008i3skNgy1gqjimjceb0j319l0mnq59.jpg)

The default ticket information is arranged in order of price. Users can choose to order in order of departure or arrival by clicking the field above the ticket information, as shown in the figure below, pay attention to the changes of the last two lines.

![iShot2021-05-15 22.54.58](https://tva1.sinaimg.cn/large/008i3skNgy1gqjijaa8a4j319l0mnq59.jpg)

If the flight database that the user needs does not exist, feedback error messages.
Modify the user's destination to China (this flight is not available in the database) for testing.

![iShot2021-05-15 23.10.09](https://tva1.sinaimg.cn/large/008i3skNgy1gqjinffii0j319a0mowfw.jpg)

### 5.3 Ticket booking interface

![iShot2021-05-15 22.50.27](../../Downloads/iShot2021-05-15%2022.50.27.png)

Since the user has not logged in, it will be fed directly to the login interface.

![iShot2021-05-15 22.51.16](https://tva1.sinaimg.cn/large/008i3skNgy1gqjiq9384fj314p0kz0tr.jpg)

Because the user has not registered yet, the user clicks Click here on the page to enter the registration account page and completes the account registration.

![iShot2021-05-15 22.53.37](https://tva1.sinaimg.cn/large/008i3skNgy1gqjir6scutj314r0lcq48.jpg)

After the user registers the account, it will be loaded directly to the query page.

![iShot2021-05-15 22.54.58](https://tva1.sinaimg.cn/large/008i3skNgy1gqjis8eir0j319l0mnq59.jpg)

The user clicks again to book the ticket. If the user has not booked the flight, load the booking confirmation page, and if the user has booked, load the booking conflict page.

![iShot2021-05-15 22.55.19](https://tva1.sinaimg.cn/large/008i3skNgy1gqjiumdwy6j319a0l2abp.jpg)

Click OK on the normal booking page to complete the booking.

![iShot2021-05-15 22.55.36](https://tva1.sinaimg.cn/large/008i3skNgy1gqjiv42sxqj319d0kstam.jpg)

Users in the personal center can view their booking information.

![iShot2021-05-15 23.22.11](https://tva1.sinaimg.cn/large/008i3skNgy1gqjixxe75zj319q0l90uh.jpg)

If the user selects the ticket he has booked, load the booking conflict page.

![iShot2021-05-15 23.10.09](https://tva1.sinaimg.cn/large/008i3skNgy1gqjiyoes88j319a0mowfw.jpg)

### 5.4 Ticket refund interface

In the user's personal center, the ticket can be refunded

![iShot2021-05-15 23.06.31](https://tva1.sinaimg.cn/large/008i3skNgy1gqjj41uhtgj319b0ng76c.jpg)

Select confirmation, complete the refund, and refresh the user's booking information.

![iShot2021-05-15 23.29.11](https://tva1.sinaimg.cn/large/008i3skNgy1gqjj5akpk5j319j0mlgmy.jpg)

## VI. Concluding remarks

Actually from the beginning of the course, I have been preparing for this database course. I learned from the seniors that the best way to implement the database course is to write a website. So when I went back from the winter vacation, I learned the basic html, css, js, had a basic understanding of the front end of the website, and combined with me. At present, the commonly used Python language has learned the Django web development framework and has a deep understanding of website back-end processing. Although the interaction between the front and back ends of the website is a little vague, after this database class, many of my questions have been answered and I am more proficient in **Python + Django + Mysql** development.

When building a ticket reservation system, the main problem is to establish entities and determine the relationship between entities. A passenger can order multiple aircraft, one aircraft can carry multiple users, and there is a many-to-many relationship between aircraft and passengers. Only when this is clear can a reasonable database be built to complete transaction requirements. .

In addition, one deep takeaway is that when developing with the web, the operation on the database is no longer SQL statements, but through the syntax of high-level languages (such as Python) to complete the addition, deletion and modification of the database.

To give a simple example, when querying all flight information in the booksystem_flight table:

- SQL: Select from booksystem_flight from *;
- Django: Flight.objects.all()

It can be seen that the combination of high-level language and database has transformed many underlying data operations into high-level language programs familiar to developers, but in any case, the database is still operated. Traditional SQL statements are still useful, which is convenient for us to verify whether the code logic is correct. In short, the harvest is very More.



